import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import firebase from "firebase/app";
import "firebase/auth";
@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private afAuth: AngularFireAuth, private router: Router) {}

  signInGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();

    return this.afAuth.signInWithPopup(provider).then(
      (res) => {
        this.router.navigate(["/home"]);
        localStorage.setItem("token", JSON.stringify(res.user?.uid));
      },
      (err) => {
        alert(err.message);
      }
    );
  }

  logout() {
    this.afAuth.signOut().then(
      () => {
        localStorage.removeItem("token");
        this.router.navigate(["/login"]);
      },
      (err) => {
        alert(err.message);
      }
    );
  }
}

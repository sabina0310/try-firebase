import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  rovider: any;
  user: any;
  constructor(private authService: AuthService) {}

  ngOnInit(): void {}

  signInGoogle() {
    this.authService.signInGoogle();
  }
}

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import firebase from "firebase/app";

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBLBA0WVj0pEDOepcK1XqHjRlMJLE-4eJA",
    authDomain: "fir-auth-db353.firebaseapp.com",
    projectId: "fir-auth-db353",
    storageBucket: "fir-auth-db353.appspot.com",
    messagingSenderId: "691142889163",
    appId: "1:691142889163:web:9f85fb903b85fd788dd54e",
    measurementId: "G-BRLGL73Q1Q",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
